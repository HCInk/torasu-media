//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#include "include/opus_wrapper.h"
#include "include/png.h"
#include "include/ogg.h"
#include "include/tm_types.h"
#include "include/mp4.h"
#include "include/audioengines.h"
#include <vector>
#include <string>
#include <chrono>
#include <thread>

int opus_encode(byte* data, size_t size) {
    auto encoder = tm_opus::Encoder(48000, 2, OPUS_APPLICATION_AUDIO);
    std::vector<opus_int16> raw;
    raw.insert(raw.end(), data[0], raw[size]);
    std::vector<std::vector<byte>> out = encoder.encode(raw, 960);
    return 0;
}

int png_decode_path(std::string path) {
    std::vector<unsigned char> image; //the raw pixels
    unsigned width, height;

    //decode
    unsigned error = png::decode(image, width, height, path.c_str());

    return error;
}
int readOggFile(std::string path) {
    tm_ogg::OggFile* result = tm_ogg::OggFile::readFromDisk(const_cast<char *>(path.c_str()));
    std::vector<uint16_t> samples = result->samples; // raw data
    return 0;
}
void playback_engine_test() {
    tm_apb_engine::AudioPlaybackEngine* engine = tm_apb_engine::AudioPlaybackEngine::play("/Users/liz3/Desktop/fdream.mp3");
    engine->setGain(.6);
    engine->preparePlay();
    int counter = 0;
    //THIS is a blocking call!
    engine->playBackSync([&counter, &engine]() mutable {
        counter++;
        if(counter == 60)
            engine->pause();
    });
    std::this_thread::sleep_for(std::chrono::seconds (15));
    engine->playBackSync([]() {

    });
    delete [] engine;
}
//more to follow...
int main() {
   // png_decode_path("/Users/liz3/Pictures/Anime_wallpapers/waifu.png");
   // tm_mp4::Mp4* file = tm_mp4::Mp4::loadFromFile("/Users/liz3/Desktop/143386147_Superstar_W.mp4");
//    tm_mp4::Mp4* file = tm_mp4::Mp4::loadFromFile("/Users/liz3/Desktop/138690026_Hit_the_flo.mp4");
//    std::vector<unsigned char> image; //the raw pixels
//    unsigned width, height;
//    //decode
//    unsigned error = png::decode(image, width, height, "/Users/liz3/Downloads/0001.png", LCT_RGBA, 16);
//    //if there's an error, display it
//    if(error) std::cout << "decoder error " << error << ": " << png_error_text(error) << std::endl;
//    for (int i = 0; i < (width * height * 8); i+=8) {
//        uint16_t r,g,b,a;
//        r = (image[i + 0] << 8) | (image[i + 1] >> 8);
//        g = (image[i + 2] << 8) | (image[i + 3] >> 8);
//        b = (image[i + 4] << 8) | (image[i + 5] >> 8);
//        a = (image[i + 6] << 8) | (image[i + 7] >> 8);
//
//        //    r = (image[i + 0] << 8) | (image[i + 1] << 8);
//
//        if(r <= 255 && g <= 255 && b <= 255) continue;
//        std::cout << r << ":" << g << ":" << b << ":" << a << "\n";
//    }
    playback_engine_test();
    return 0;
}