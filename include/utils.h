//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#ifndef TORASU_MEDIA_UTILS_H
#define TORASU_MEDIA_UTILS_H

#include "tm_types.h"
#include <string>

static void* tm_malloc(size_t size) {
  return malloc(size);
}
static void* tm_realloc(void* ptr, size_t new_size) {
  return realloc(ptr, new_size);
}

static void tm_free(void* ptr) {
  free(ptr);
}

static void string_cleanup(char** out) {
  tm_free(*out);
  *out = NULL;
}

static void tm_memcpy(void* TM_RESTRICT dst,
                      const void* TM_RESTRICT src, size_t size) {
  size_t i;
  for(i = 0; i < size; i++) ((char*)dst)[i] = ((const char*)src)[i];
}

static void tm_memset(void* TM_RESTRICT dst,
                      int value, size_t num) {
  size_t i;
  for(i = 0; i < num; i++) ((char*)dst)[i] = (char)value;
}

static char* alloc_string_sized(const char* in, size_t insize) {
  char* out = (char*)tm_malloc(insize + 1);
  if(out) {
      tm_memcpy(out, in, insize);
    out[insize] = 0;
  }
  return out;
}
static size_t tm_strlen(const char* a) {
  const char* orig = a;
  /* avoid warning about unused function in case of disabled COMPILE... macros */
  (void)(&tm_strlen);
  while(*a) a++;
  return (size_t)(a - orig);
}

/* dynamically allocates a new string with a copy of the null terminated input text */
static char* alloc_string(const char* in) {
  return alloc_string_sized(in, tm_strlen(in));
}

static bool tm_isBigEndian() {
    int num = 1;
    return *(char *) &num != 1;
}

#endif //TORASU_MEDIA_UTILS_H
