//
// Created by Yann Holme Nielsen on 24/05/2020.
//

#ifndef TORASU_MEDIA_MP4_H
#define TORASU_MEDIA_MP4_H

#include "tm_types.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "utils.h"
#include <map>


namespace tm_mp4 {

    typedef struct _matrix
    {
        float a;
        float b;
        float u;
        float c;
        float d;
        float v;
        float x;
        float y;
        float w;
    } matrix;
    typedef struct _hint_header {
        uint16_t maxPDUsize;
        uint16_t avgPDUsize;
        uint32_t maxbitrate;
        uint32_t avgbitrate;
    } hint_header;
    typedef struct _mvhd_v1 {
        uint32_t creation_time;
        uint32_t modification_time;
        uint32_t timescale;
        uint32_t duration;
        float rate;
        float volume;
        matrix matrix;

    } mvhd_v1;
    typedef struct _track_edts_entry{
        uint32_t segment_duration;
        int32_t media_time;
        int16_t media_rate_integer;
        int16_t media_rate_fraction;
    } track_edts_entry;
    typedef struct _track_edts{
        std::vector<track_edts_entry> entries;
    } track_edts;

    typedef struct _mdia{
        uint32_t creation_time;
        uint32_t modification_time;
        uint32_t timescale;
        uint32_t duration;
        std::string lang;
        std::string handler_type; //vide, soun, hint
        std::string name;

        hint_header* h_header = nullptr;

        //video
        uint16_t graphicsMode;
        uint16_t opColor[3];

        //audio
        int16_t balance;

        std::vector<std::string> urls;

    } mdia;

    typedef struct _track{
        uint32_t creation_time;
        uint32_t modification_time;
        uint32_t track_id;
        uint32_t duration;
        uint16_t layer;
        uint16_t alternate_group;
        uint16_t volume;
        matrix trans_matrix;
        float width;
        float height;
        track_edts* edts = nullptr;
        mdia* m;
    } track;
    class Mp4 {
    public:
        static Mp4* loadFromFile(char* filepath);
        mvhd_v1* header = nullptr;
        std::vector<std::string> supportedFormats;
    private:
        static mvhd_v1* readMvhdHeader(std::ifstream* stream, bool is) {
            auto version = Mp4::readUnsignedU8(stream, is);
            if(version != 0) return nullptr;
            tm_mp4::Mp4::skip(stream, 3);
            mvhd_v1* header = new mvhd_v1();
            header->creation_time = Mp4::readUnsignedU32(stream, is);
            header->modification_time = Mp4::readUnsignedU32(stream, is);
            header->timescale = Mp4::readUnsignedU32(stream, is);
            header->duration = Mp4::readUnsignedU32(stream, is);
            header->rate = Mp4::readBigEndianFixedPoint(stream, 16,16, is);
            header->volume = Mp4::readBigEndianFixedPoint(stream,8,8, is);
            Mp4::skip(stream, 10);
            Mp4::readMatrix(&(header->matrix), stream, is);
            Mp4::skip(stream, 24);
            return header;
        }
        static std::string * readBigEndianISO639Code(std::ifstream* stream, bool is)
        {
            unsigned char c1;
            unsigned char c2;
            unsigned char c3;
            uint16_t      n;
            std::string * s;

            s = new std::string();
            n = Mp4::readUnsignedU16(stream, is);

            c1 = ( n & 0x7C00 ) >> 10;  // Mask is 0111 1100 0000 0000
            c2 = ( n & 0x03E0 ) >> 5;   // Mask is 0000 0011 1110 0000
            c3 = ( n & 0x001F );        // Mask is 0000 0000 0001 1111

            c1 += 0x60;
            c2 += 0x60;
            c3 += 0x60;

            s->append( ( char * )&c1, 1 );
            s->append( ( char * )&c2, 1 );
            s->append( ( char * )&c3, 1 );

            return s;
        }
        static track_edts* readTrackEdit(std::ifstream* stream, bool is) {
            auto subSize = Mp4::readUnsignedU32(stream, is);
            auto subType = Mp4::readString(stream, 4);
            auto version = Mp4::readUnsignedU8(stream, is);
            if(version != 0) {
                tm_mp4::Mp4::skip(stream, subSize - 9);
                return nullptr;
            }
            tm_mp4::Mp4::skip(stream, 3);
            track_edts* edts = new track_edts();
            auto entryCount = Mp4::readUnsignedU32(stream, is);
            for (int i = 0; i < entryCount; ++i) {
                track_edts_entry entry;
                entry.segment_duration = Mp4::readUnsignedU32(stream, is);
                entry.media_time = Mp4::readSigned32(stream, is);
                entry.media_rate_integer = Mp4::readSigned16(stream, is);
                entry.media_rate_fraction =  Mp4::readSigned16(stream, is);
                edts->entries.push_back(entry);
            }
            return edts;
        }

        static void readMinf(uint32_t  length, std::ifstream* stream, bool is, mdia* m) {
            int localLen = length;
            while (localLen > 0) {
                auto subSize = Mp4::readUnsignedU32(stream, is);
                auto subType = Mp4::readString(stream, 4);
                if(subType == "vmhd" || subType == "smhd" || subType == "hmhd" || subType == "nmhd") {
                    Mp4::skip(stream, 4);
                    if(subType == "vmhd") {
                        m->graphicsMode = Mp4::readUnsignedU16(stream, is);
                        m->opColor[0] = Mp4::readUnsignedU16(stream, is);
                        m->opColor[1] = Mp4::readUnsignedU16(stream, is);
                        m->opColor[2] = Mp4::readUnsignedU16(stream, is);
                    } else if(subType == "smhd") {
                        m->balance = Mp4::readSigned16(stream, is);
                        Mp4::skip(stream, 2);
                    } else if (subType == "hmhd") {
                        hint_header * h = new hint_header ();
                        h->maxPDUsize = Mp4::readUnsignedU16(stream, is);
                        h->avgPDUsize = Mp4::readUnsignedU16(stream, is);
                        h->avgbitrate = Mp4::readUnsignedU32(stream, is);
                        h->maxbitrate = Mp4::readUnsignedU32(stream, is);
                        Mp4::skip(stream, 4);
                    }
                    else {
                        Mp4::skip(stream, subSize - 8 - 4);

                    }
                } else if(subType == "dinf") {
                    auto dinfSize = Mp4::readUnsignedU32(stream, is);
                    auto dinfType = Mp4::readString(stream, 4);
                   std::cout << "dinf: " << dinfType << "\n";
                    if(dinfType == "dref") {
                        Mp4::skip(stream, 4);
                        auto entryCount = Mp4::readUnsignedU32(stream, is);
                        for (int i = 0; i < entryCount; ++i) {
                            auto subDinfSize = Mp4::readUnsignedU32(stream, is);
                            auto subDinfType = Mp4::readString(stream, 4);
                            if(subDinfType == "url ") {
                                if(subDinfSize > 12) {
                                    Mp4::skip(stream, 4);

                                    std::vector<char> set;
                                    while (true){
                                        char read = stream->get();
                                        if(read == '\0') break;
                                        set.push_back(read);
                                    }
                                    m->urls.push_back(std::string(&set[0], set.size()));

                                } else {
                                    m->urls.push_back("");

                                    Mp4::skip(stream, 4);
                                }
                            } else if(subDinfType == "urn ") {
                                Mp4::skip(stream, 4);

                                std::vector<char> set;
                                while (true){
                                    char read = stream->get();
                                    if(read == '\0') break;
                                    set.push_back(read);
                                }
                                auto name = std::string(&set[0], set.size());
                                set.clear();
                                while (true){
                                    char read = stream->get();
                                    if(read == '\0') break;
                                    set.push_back(read);
                                }
                                auto url = std::string(&set[0], set.size());
                                m->urls.push_back(name + ":" + url);

                            } else {
                                Mp4::skip(stream, subDinfSize - 8);
                            }
                        }
                    } else {
                        Mp4::skip(stream, dinfSize - 8);
                    }
                }else {
                    std::cout << "TT: " << subType << "\n";
                    Mp4::skip(stream, subSize - 8);
                }

                localLen -= subSize;
            }
        }
        static mdia *readMediaDeclaration(uint32_t subLen, std::ifstream* stream, bool is) {
            auto localLen = subLen;
            mdia* m = new mdia ();
            while (localLen > 0) {
                auto subSize = Mp4::readUnsignedU32(stream, is);
                auto subType = Mp4::readString(stream, 4);
                if (subType == "mdhd") {
                    auto version = Mp4::readUnsignedU8(stream, is);
                    tm_mp4::Mp4::skip(stream, 3);
                    if(version == 0) {
                        m->creation_time = Mp4::readUnsignedU32(stream, is);
                        m->modification_time = Mp4::readUnsignedU32(stream, is);
                        m->timescale = Mp4::readUnsignedU32(stream, is);
                        m->duration = Mp4::readUnsignedU32(stream, is);
                        m->lang = std::string(Mp4::readBigEndianISO639Code(stream, is)->c_str());
                        Mp4::skip(stream, 2);

                    } else {
                        Mp4::skip(stream, subSize - 8 - 1 -3);
                    }
                } else if(subType == "hdlr") {
                    tm_mp4::Mp4::skip(stream, 4);
                    tm_mp4::Mp4::skip(stream, 4);
                    m->handler_type = Mp4::readString(stream, 4);
                    tm_mp4::Mp4::skip(stream, 12);
                    std::vector<char> set;
                    while (true){
                        char read = stream->get();
                        if(read == '\0') break;
                        set.push_back(read);
                    }
                    m->name = std::string(&set[0], set.size());
                } else if(subType == "minf") {
                    readMinf(subSize - 8, stream, is, m);
                }else {
                    std::cout << "\t\t\t\t" << subType << "\n";
                    Mp4::skip(stream, subSize - 8);
                }
                localLen -= subSize;

            }
            return m;
        }

       static track* readTrack(uint32_t length, std::ifstream* stream, bool is) {
            track* tr = new track ();
            int localLength = length;
            while (localLength > 0) {
                auto subSize = Mp4::readUnsignedU32(stream, is);
                auto subType = Mp4::readString(stream, 4);
               // localLength -= 8;
                if(subType == "tkhd") {
                    auto version = Mp4::readUnsignedU8(stream, is);
                    tm_mp4::Mp4::skip(stream, 3);

                    if(version == 0) {
                        tr->creation_time = Mp4::readUnsignedU32(stream, is);
                        tr->modification_time = Mp4::readUnsignedU32(stream, is);
                        tr->track_id = Mp4::readUnsignedU32(stream, is);
                        Mp4::skip(stream, 4);
                        tr->duration = Mp4::readUnsignedU32(stream, is);
                        Mp4::skip(stream, 8);
                        tr->layer = Mp4::readUnsignedU16(stream, is);
                        tr->alternate_group = Mp4::readUnsignedU16(stream, is);
                        tr->volume = Mp4::readUnsignedU16(stream, is);
                        Mp4::skip(stream, 2);
                        Mp4::readMatrix(&(tr->trans_matrix), stream, is);
                        tr->width = Mp4::readBigEndianFixedPoint(stream, 16,16, is);
                        tr->height = Mp4::readBigEndianFixedPoint(stream, 16,16, is);

                    }else {
                        Mp4::skip(stream, subSize - 9);
                        return nullptr;
                    }
                    localLength -= subSize;
                } else if (subType == "edts" && subSize > 8) {

                    tr->edts = readTrackEdit(stream, is);
                    localLength -= subSize;
                }else if (subType == "mdia") {

                    tr->m = readMediaDeclaration(subSize - 8, stream, is);
                    localLength -= subSize;
                } else {
                    Mp4::skip(stream, subSize - 8);
                    localLength -= subSize;
                }
                std::cout << "\t\t\t" << subType << "\n";
            }
            return tr;
        }

        static void skip(std::ifstream* stream, int length) {
            int curr = stream->tellg();
            stream->seekg(curr + length);
        }
        static std::string readString(std::ifstream * stream, int length) {
            char* data = new char[length];
            stream->read(reinterpret_cast<char *>(data), length);
            std::string str(data, length);
            delete [] data;
            return str;
        }
        static uint32_t readUnsignedU32(std::ifstream *stream, bool b) {
            uint32_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
            if(!b) data = __builtin_bswap32(data);
            return data;
        }
        static float readBigEndianFixedPoint(std::ifstream* stream, unsigned int integerLength, unsigned int fractionalLength, bool is)
        {
            uint32_t n;
            unsigned int integer;
            unsigned int fractionalMask;
            unsigned int fractional;

            if( integerLength + fractionalLength == 16 )
            {
                n = Mp4::readUnsignedU16(stream, is);
            }
            else
            {
                n = Mp4::readUnsignedU32(stream, is);
            }

            integer        = n >> fractionalLength;
            fractionalMask = pow( 2, fractionalLength ) - 1;
            fractional     = ( n & fractionalMask ) / ( 1 << fractionalLength );

            return integer + fractional;
        }
        static void readMatrix( matrix * m, std::ifstream* stream, bool is )
        {
            m->a = Mp4::readBigEndianFixedPoint(stream, 16, 16 , is);
            m->b = Mp4::readBigEndianFixedPoint(stream,  16, 16, is );
            m->u = Mp4::readBigEndianFixedPoint(stream,  2, 3, is );
            m->c = Mp4::readBigEndianFixedPoint(stream, 16, 16, is );
            m->d = Mp4::readBigEndianFixedPoint(stream, 16, 16, is );
            m->v = Mp4::readBigEndianFixedPoint(stream,  2, 30, is );
            m->x = Mp4::readBigEndianFixedPoint(stream, 16, 16, is);
            m->y = Mp4::readBigEndianFixedPoint(stream, 16, 16, is );
            m->w = Mp4::readBigEndianFixedPoint(stream,  2, 30, is);
        }
        static uint16_t readUnsignedU16(std::ifstream *stream, bool b) {
            uint16_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
            if(!b) data = __builtin_bswap16(data);
            return data;
        }
        static uint8_t readUnsignedU8(std::ifstream *stream, bool b) {
            uint8_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
            return data;
        }
        static int16_t readSigned16(std::ifstream *stream, bool b) {
            int16_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
         //   if(!b) data = __builtin_bswap16(data);
            return data;
        }
        static int32_t readSigned32(std::ifstream *stream, bool b) {
            int32_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
          //  if(!b) data = __builtin_bswap16(data);
            return data;
        }

        static uint64_t readUnsignedU64(std::ifstream *stream, bool b) {
            uint64_t data;
            stream->read(reinterpret_cast<char *>(&data), sizeof(data));
            if(!b) data = __builtin_bswap64(data);
            return data;
        }
    };
}
#endif //TORASU_MEDIA_MP4_H
