//
// Created by Yann Holme Nielsen on 07/06/2020.
//

#ifndef TORASU_MEDIA_AUDIOENGINES_H
#define TORASU_MEDIA_AUDIOENGINES_H

#include "utils.h"


#ifdef __APPLE__

#include <AudioToolbox/AudioToolbox.h>
#include <CoreFoundation/CoreFoundation.h>

#endif
namespace tm_apb_engine {
using PlayCallback = std::function<void()>;

#ifdef __APPLE__
    static const int kNumberBuffers = 3;                              // 1
    struct AQPlayerState {
        AudioStreamBasicDescription mDataFormat;                    // 2
        AudioQueueRef mQueue;                         // 3
        AudioQueueBufferRef mBuffers[kNumberBuffers];       // 4
        AudioFileID mAudioFile;                     // 5
        UInt32 bufferByteSize;                 // 6
        SInt64 mCurrentPacket;                 // 7
        UInt32 mNumPacketsToRead;              // 8
        AudioStreamPacketDescription *mPacketDescs;                  // 9
        bool mIsRunning;                     // 10
    };

    static void HandleOutputBuffer(
            void *aqData,
            AudioQueueRef inAQ,
            AudioQueueBufferRef inBuffer
    ) {

//    std::cout << "cb" << std::endl;

        OSStatus status;

        AQPlayerState *pAqData = (AQPlayerState *) aqData;        // 1
        if (pAqData->mIsRunning == 0) return;                     // 2
        UInt32 numBytesReadFromFile;                              // 3
        UInt32 numPackets = pAqData->mNumPacketsToRead;           // 4
        status = AudioFileReadPackets(
                pAqData->mAudioFile,
                false,
                &numBytesReadFromFile,
                pAqData->mPacketDescs,
                pAqData->mCurrentPacket,
                &numPackets,
                inBuffer->mAudioData
        );
        if (numPackets > 0) {                                     // 5
            inBuffer->mAudioDataByteSize = numBytesReadFromFile;  // 6
            status = AudioQueueEnqueueBuffer(
                    pAqData->mQueue,
                    inBuffer,
                    (pAqData->mPacketDescs ? numPackets : 0),
                    pAqData->mPacketDescs
            );
            pAqData->mCurrentPacket += numPackets;                // 7
        } else {
            status = AudioQueueStop(
                    pAqData->mQueue,
                    false
            );
            pAqData->mIsRunning = false;
        }
    }

#endif

    class AudioPlaybackEngine {
    private:
        bool playing;
        char *file_path;

        void prepare();

        double timeBase;
        float gain = 1.0;
#ifdef __APPLE__
        AQPlayerState aqData;

        static void deriveBufferSize(
                AudioStreamBasicDescription &ASBDesc,
                UInt32 maxPacketSize,
                Float64 seconds,
                UInt32 *outBufferSize,
                UInt32 *outNumPacketsToRead
        ) {
            static const int maxBufferSize = 0x50000;
            static const int minBufferSize = 0x4000;

            if (ASBDesc.mFramesPerPacket != 0) {
                Float64 numPacketsForTime =
                        ASBDesc.mSampleRate / ASBDesc.mFramesPerPacket * seconds;
                *outBufferSize = numPacketsForTime * maxPacketSize;
            } else {
                *outBufferSize =
                        maxBufferSize > maxPacketSize ?
                        maxBufferSize : maxPacketSize;
            }

            if (*outBufferSize > maxBufferSize &&
                *outBufferSize > maxPacketSize
                    )
                *outBufferSize = maxBufferSize;
            else {
                if (*outBufferSize < minBufferSize)
                    *outBufferSize = minBufferSize;
            }

            *outNumPacketsToRead = *outBufferSize / maxPacketSize;
        }

        void primeBuffer() {
            OSStatus status;
            for (int i = 0; i < kNumberBuffers; ++i) {
                status = AudioQueueAllocateBuffer(
                        aqData.mQueue,
                        aqData.bufferByteSize,
                        &aqData.mBuffers[i]
                );
                HandleOutputBuffer(
                        &aqData,
                        aqData.mQueue,
                        aqData.mBuffers[i]
                );
            }

#if 1
            status = AudioQueuePrime(
                    aqData.mQueue,
                    kNumberBuffers,
                    NULL
            );
#endif
        }

        void seekToPacket(uint64_t packet) {
            AudioQueueStop(aqData.mQueue, true);
            aqData.mCurrentPacket = packet;
            primeBuffer();
            AudioQueueStart(aqData.mQueue, NULL);

        }

#endif
    public:
        AudioPlaybackEngine(char *file_path);
        static AudioPlaybackEngine *play(char *path);
        double getProgress();
        void seek(double sec);
        bool isPlaying();
        void setGain(float targetGain);
        void preparePlay();
        void playBackSync(const PlayCallback &cb);
        void pause();
    };

}

#endif //TORASU_MEDIA_AUDIOENGINES_H
