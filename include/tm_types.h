//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#ifndef TORASU_MEDIA_TM_TYPES_H
#define TORASU_MEDIA_TM_TYPES_H

typedef unsigned char byte;
/* restrict is not available in C90, but use it when supported by the compiler */
#if (defined(__GNUC__) && (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1))) ||\
    (defined(_MSC_VER) && (_MSC_VER >= 1400)) || \
    (defined(__WATCOMC__) && (__WATCOMC__ >= 1250) && !defined(__cplusplus))
#define TM_RESTRICT __restrict
#else
#define TM_RESTRICT /* not available */
#endif

#if (defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)) || (defined(__cplusplus) && (__cplusplus >= 199711L))
#define TM_INLINE inline
#else
#define TM_INLINE /* not available */
#endif

#endif //TORASU_MEDIA_TM_TYPES_H
