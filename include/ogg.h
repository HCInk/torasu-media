//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#ifndef TORASU_MEDIA_OGG_H
#define TORASU_MEDIA_OGG_H
#include <vector>
#include <opusfile.h>
#include "tm_types.h"

namespace tm_ogg {
class OggHandle {
 public:
  OggOpusFile* f;
};
class OggFile {
 public:
  static OggFile* readFromDisk(char* path);
  static OggFile* readFromStream(byte* data,size_t size);
  static int8_t saveToDisk(byte* data,size_t size, uint32_t sampleRate, uint8_t channelCount);
  static bool test(OggHandle* handle);
  std::vector<uint16_t> samples;
  uint32_t sample_rate;
  int32_t channel_count;

};
}
#endif //TORASU_MEDIA_OGG_H
