//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#ifndef TORASU_MEDIA_WAV_H
#define TORASU_MEDIA_WAV_H

#include <string>
#include <fstream>
#include "tm_types.h"
#include "utils.h"
#include "stream_util.h"
#include <vector>
namespace tm_wav {
struct UnknownChunk {
  size_t size;
  std::vector<byte> data;
  char type[5];

};
class WavFile {
 public:
  static tm_wav::WavFile* readFromDisk(char* path);
  static tm_wav::WavFile* readFromBuffer(char* data, size_t size);
  int16_t chanelAmount;
  int32_t sampleRate;
  int32_t byteRate;
  int16_t blockAlign;
  int16_t bitsPerSample;
  std::vector<byte> audio_data;
  std::vector<UnknownChunk> otherChunks;
 private:
  static tm_wav::WavFile* readFromStream(std::istream* stream);
};
}

#endif //TORASU_MEDIA_WAV_H
