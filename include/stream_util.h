//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#ifndef TORASU_MEDIA_STREAM_UTIL_H
#define TORASU_MEDIA_STREAM_UTIL_H
#include <streambuf>

struct membuf : std::streambuf {
  membuf(char* begin, char* end) {
    this->setg(begin, begin, end);
  }
};

#endif //TORASU_MEDIA_STREAM_UTIL_H
