//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#include "../include/ogg.h"

namespace tm_ogg {

OggFile *OggFile::readFromDisk(char *path) {
  int *opus_err = nullptr;
  OggOpusFile *file = op_open_file(path, opus_err);
  auto head = op_head(file, 0);
  auto *f = new OggFile();
  f->channel_count = head->channel_count;
  f->sample_rate = head->input_sample_rate;
  while (true) {
    int size = 48 * 60;
    auto *buff = new opus_int16[size];
    int o = op_read(file, buff, size, nullptr);
    if (o <= 0) break;
    f->samples.insert(f->samples.end(), &buff[0], &buff[o]);
  }
  op_free(file);
  return f;
}

bool OggFile::test(OggHandle *handle) {
  int result = op_test_open(handle->f);
  return result == 0;
}

OggFile *OggFile::readFromStream(byte *data, size_t size) {
  int *opus_err = nullptr;
  OggOpusFile *file = op_open_memory(data, size, opus_err);
  auto head = op_head(file, 0);
  auto *f = new OggFile();
  f->channel_count = head->channel_count;
  f->sample_rate = head->input_sample_rate;
  while (true) {
    int size = 48 * 60;
    auto *buff = new opus_int16[size];
    int o = op_read(file, buff, size, nullptr);
    if (o <= 0) break;
    f->samples.insert(f->samples.end(), &buff[0], &buff[o]);
  }
  op_free(file);
  return f;

}

int8_t OggFile::saveToDisk(byte *data, size_t size, uint32_t sampleRate, uint8_t channelCount) {

  return 0;
}
}