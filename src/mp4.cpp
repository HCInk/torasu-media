//
// Created by Yann Holme Nielsen on 24/05/2020.
//

#include "../include/mp4.h"

tm_mp4::Mp4* tm_mp4::Mp4::loadFromFile(char* filepath) {
    bool is = tm_isBigEndian();
    std::ifstream* stream = new std::ifstream(filepath);
    stream->seekg (0, stream->end);
    int64_t totalLength = stream->tellg();
    stream->seekg (0, stream->beg);
    Mp4* endFile = new Mp4();
    while (true) {
        if ((totalLength - stream->tellg()) == 0) break;
        auto size = Mp4::readUnsignedU32(stream, is);
        auto type = Mp4::readString(stream, 4);
        if(size == 0)  {
            std::cout << (totalLength - stream->tellg())<< ":BREAK:"   << "\n";
            break;
        }
        if(size == 1) {
            auto realSize = Mp4::readUnsignedU64(stream, is);
            Mp4::skip(stream, realSize);
            std::cout << realSize<< "-BIG:" << type << "\n";
        } else {
            std::cout << size<< ":" << type << "\n";
            if(type == "ftyp"){
                int lengthOfBrands = size - 8;
                std::string identifier = Mp4::readString(stream, 4);
                auto brandVersion = Mp4::readUnsignedU32(stream, is);
                lengthOfBrands -= 8;
                for (int i = 0; i < (lengthOfBrands / 4); ++i) {
                    std::string entry = Mp4::readString(stream, 4);
                    endFile->supportedFormats.push_back(entry);
                    std::cout << "\tsupported entry: " << entry << "\n";
                }

            } else if(type == "moov") {
                while (true) {
                    if ((totalLength - stream->tellg()) == 0) break;
                    auto subSize = Mp4::readUnsignedU32(stream, is);
                    auto subType = Mp4::readString(stream, 4);
                    if(subSize == 0)  {
                        std::cout << "moov block end" << "\n";
                        break;
                    }
                    if(subType == "mvhd") {
                       tm_mp4::mvhd_v1* h = Mp4::readMvhdHeader(stream, is);
                       endFile->header = h;
                       auto nextTrackId = Mp4::readUnsignedU32(stream, is);

                    } else if(subType == "trak") {
                        Mp4::readTrack(subSize - 8, stream, is);
                    } else {

                    Mp4::skip(stream, subSize - 8);
                    }
                    std::cout << "\t" <<  subSize<< ":" << subType << "\n";
                }

            }  else {
                Mp4::skip(stream, size - 8);
            }


        }
    }
    return endFile;
}
