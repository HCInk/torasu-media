//
// Created by Yann Holme Nielsen on 21/05/2020.
//

#include "../include/wav.h"
namespace tm_wav {
WavFile* tm_wav::WavFile::readFromDisk(char *path) {
  auto* stream = new std::ifstream(path);
  WavFile* r = readFromStream(stream);
  stream->close();
  delete (stream);
  return r;
}
WavFile* tm_wav::WavFile::readFromBuffer(char *data, size_t size) {
  membuf sbuf(data, &data[size]);
  auto* stream = new std::istream (&sbuf);
  WavFile* r = readFromStream(stream);
  delete (stream);
  return r;
}
WavFile* tm_wav::WavFile::readFromStream(std::istream* stream) {
  stream->seekg (0, stream->end);
  int totalLength = stream->tellg();
  stream->seekg (0, stream->beg);
  char header [4];
  stream->read(header, 4);
  std::string headerStr = std::string(header, 4);
  auto file = new WavFile();
  if(headerStr != "RIFF") return nullptr;
  int chunkSize;
  stream->read(reinterpret_cast<char *>(&chunkSize), sizeof(chunkSize));
  char wave[4];
  stream->read(wave, 4);
  char fmt[4];
  stream->read(fmt, 4);
  int Subchunk1Size;
  int16_t audioFormat;
  int16_t chanelAmount;
  int32_t sampleRate;
  int32_t byteRate;
  int16_t blockAlign;
  int16_t bitsPerSample;
  stream->read(reinterpret_cast<char *>(&Subchunk1Size), sizeof(Subchunk1Size));
  stream->read(reinterpret_cast<char *>(&audioFormat), sizeof(audioFormat));
  stream->read(reinterpret_cast<char *>(&chanelAmount), sizeof(chanelAmount));
  stream->read(reinterpret_cast<char *>(&sampleRate), sizeof(sampleRate));
  stream->read(reinterpret_cast<char *>(&byteRate), sizeof(byteRate));
  stream->read(reinterpret_cast<char *>(&blockAlign), sizeof(blockAlign));
  stream->read(reinterpret_cast<char *>(&bitsPerSample), sizeof(bitsPerSample));
  file->chanelAmount =chanelAmount;
  file->sampleRate = sampleRate;
  file->byteRate = byteRate;
  file->blockAlign = blockAlign;
  file->bitsPerSample = bitsPerSample;
  while (true) {
    if(stream->tellg() >= totalLength) break;
    char identifier[4];
    stream->read(identifier, 4);
    std::string identifierStr = std::string(identifier, 4);
    stream->read(reinterpret_cast<char *>(&chunkSize), sizeof(chunkSize));
    if(identifierStr == "data") {
      byte* datap = (byte*) tm_malloc(chunkSize);
      stream->read(reinterpret_cast<char *>(datap), chunkSize);
      file->audio_data.insert(file->audio_data.end(),&datap[0], &datap[chunkSize]);
      tm_free(datap);
    } else {
      UnknownChunk chunk;
      byte* datap = (byte*) tm_malloc(chunkSize);
      stream->read(reinterpret_cast<char *>(datap), chunkSize);
      chunk.data.insert(chunk.data.end(),&datap[0], &datap[chunkSize]);
      chunk.size = chunkSize;
      for (int i = 0; i < 4; ++i)
        chunk.type[i] = identifierStr.c_str()[i];
      chunk.type[4] = '\0';
      file->otherChunks.push_back(chunk);
      tm_free(datap);
    }
  }
  return file;
}
}