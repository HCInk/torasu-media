//
// Created by Yann Holme Nielsen on 07/06/2020.
//

#include "../include/audioengines.h"

namespace tm_apb_engine {
    AudioPlaybackEngine *AudioPlaybackEngine::play(char *path) {
        return new AudioPlaybackEngine(path);
    }

    bool AudioPlaybackEngine::isPlaying() {
#ifdef __APPLE__
        return aqData.mIsRunning;
#else
        return playing;
#endif
    }

    AudioPlaybackEngine::AudioPlaybackEngine(char *file_path) {
        this->file_path = file_path;
        playing = false;
        prepare();
    }

    void AudioPlaybackEngine::prepare() {
        timeBase = 0;
#ifdef __APPLE__
        CFURLRef url = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8 *) file_path, strlen(file_path), false);
        OSStatus status;
        memset(&aqData, 0, sizeof(aqData));

        status = AudioFileOpenURL(url, kAudioFileReadPermission, 0, &aqData.mAudioFile);
        if (status != noErr) return;
        UInt32 dataFormatSize = sizeof(aqData.mDataFormat);
        status = AudioFileGetProperty(
                aqData.mAudioFile,
                kAudioFilePropertyDataFormat,
                &dataFormatSize,
                &aqData.mDataFormat
        );
        status = AudioQueueNewOutput(
                &aqData.mDataFormat,
                HandleOutputBuffer,
                &aqData,
                CFRunLoopGetCurrent(),
                kCFRunLoopCommonModes,
                0,
                &aqData.mQueue
        );
        UInt32 maxPacketSize;
        UInt32 propertySize = sizeof(maxPacketSize);
        status = AudioFileGetProperty(
                aqData.mAudioFile,
                kAudioFilePropertyPacketSizeUpperBound,
                &propertySize,
                &maxPacketSize
        );
        deriveBufferSize(
                aqData.mDataFormat,
                maxPacketSize,
                0.5,
                &aqData.bufferByteSize,
                &aqData.mNumPacketsToRead
        );


        bool isFormatVBR = (
                aqData.mDataFormat.mBytesPerPacket == 0 ||
                aqData.mDataFormat.mFramesPerPacket == 0
        );

        if (isFormatVBR) {
            aqData.mPacketDescs =
                    (AudioStreamPacketDescription *) malloc(
                            aqData.mNumPacketsToRead * sizeof(AudioStreamPacketDescription)
                    );
        } else {
            aqData.mPacketDescs = NULL;
        }
        UInt32 cookieSize = sizeof(UInt32);
        OSStatus couldNotGetProperty =
                AudioFileGetPropertyInfo(
                        aqData.mAudioFile,
                        kAudioFilePropertyMagicCookieData,
                        &cookieSize,
                        NULL
                );
        if (!couldNotGetProperty && cookieSize) {
            char *magicCookie =
                    (char *) malloc(cookieSize);

            status = AudioFileGetProperty(
                    aqData.mAudioFile,
                    kAudioFilePropertyMagicCookieData,
                    &cookieSize,
                    magicCookie
            );
            status = AudioQueueSetProperty(
                    aqData.mQueue,
                    kAudioQueueProperty_MagicCookie,
                    magicCookie,
                    cookieSize
            );

            tm_free(magicCookie);
        }


#endif
    }

    void AudioPlaybackEngine::preparePlay() {
#ifdef __APPLE__
        OSStatus status;

        aqData.mIsRunning = true;
        aqData.mCurrentPacket = 0;

        primeBuffer();
        // Optionally, allow user to override gain setting here
        status = AudioQueueSetParameter(
                aqData.mQueue,
                kAudioQueueParam_Volume,
                gain
        );
#endif
    }

    void AudioPlaybackEngine::setGain(float targetGain) {
        gain = targetGain;
    }

    double AudioPlaybackEngine::getProgress() {
#ifdef __APPLE__
        double p;
        AudioTimeStamp timeStamp;
        OSStatus status = AudioQueueGetCurrentTime(
                aqData.mQueue,
                NULL,
                &timeStamp,
                NULL
        );
        p = timeStamp.mSampleTime / aqData.mDataFormat.mSampleRate + timeBase;
        return p;
#else
        return 0;
#endif
    }

    void AudioPlaybackEngine::seek(double sec) {
#ifdef __APPLE__
        double frame = sec * aqData.mDataFormat.mSampleRate;

        AudioFramePacketTranslation trans;
        trans.mFrame = frame;

        unsigned int sz = sizeof(trans);
        OSStatus status = AudioFileGetProperty(aqData.mAudioFile, kAudioFilePropertyFrameToPacket, (UInt32 *) &sz,
                                               &trans);

        seekToPacket(trans.mPacket);
        trans.mFrameOffsetInPacket = 0; // Don't support sub packet seeking..

        status = AudioFileGetProperty(aqData.mAudioFile, kAudioFilePropertyPacketToFrame, (UInt32 *) &sz, &trans);

        timeBase = trans.mFrame / aqData.mDataFormat.mSampleRate;

#endif
    }

    void AudioPlaybackEngine::playBackSync(const PlayCallback &cb) {
#ifdef __APPLE__
        this->aqData.mIsRunning = true;
        AudioQueueStart(
                aqData.mQueue,
                NULL
        );
        do {
            CFRunLoopRunInMode(
                    kCFRunLoopDefaultMode,
                    0.25,
                    false
            );
            cb();
        } while (isPlaying());
#endif
    }

    void AudioPlaybackEngine::pause() {
#ifdef __APPLE__
        if (isPlaying()) {
            AudioQueuePause(aqData.mQueue);
            aqData.mIsRunning = false;
        }
#endif
    }

}