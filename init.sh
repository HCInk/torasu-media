#!/bin/sh

if [ $(uname) == "Darwin" ]
then
    brew install autoconf automake libtool
fi

git submodule update --init
cd third-party/opus
./autogen.sh
./configure
make
cd ../opusfile
./autogen.sh
if [ $(uname) == "Darwin" ]
then
    PKG_CONFIG_PATH="/usr/local/opt/openssl@1.1/lib/pkgconfig" ./configure
else
    ./configure
fi
make
cd ../ogg
./autogen.sh
./configure
make