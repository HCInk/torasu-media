# Torasu Media
This is a fill suite library for working with media formats.

main.cpp holds some examples

## Building
`./init.sh`

## Supported Formats

### Image
- PNG: encode/decode
- JPEG: encode/decode

### Audio
- Wav: decode
- Opus: encode/decode
- Ogg(Opus in ogg container): decode 
- Playback: macOS using native apis

### Video
- Mp4 Container parsing(WIP), [see here](https://web.archive.org/web/20180219054429/http://l.web.umkc.edu/lizhu/teaching/2016sp.video-communication/ref/mp4.pdf)

## Libraries used
Note a lot of the codecs are taken from other projects.   
I will try to respect their licenses and give them appropriate credit/acknowledgement.

- [lodepng](https://github.com/lvandeve/lodepng): Provides PNG encoding/decoding
- [jpeg-compressor](https://github.com/richgel999/jpeg-compressor): Provides JPEG encoding/decoding
- [opuscpp](https://github.com/google/opuscpp): Wrapper around liboups